import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8000/api/book/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getAll(params : any): Observable<any> {
    
    return this.http.get(API_URL + 'all', { responseType: 'json', params : params });
  }

  create(name : string, pages : number): Observable<any> {
    return this.http.post(API_URL + 'create', {
      name,
      pages
    }, httpOptions);
  }

  get(id : number): Observable<any> {
    return this.http.get(API_URL + 'read/' + id, { responseType : 'json' });
  }

  update(id : number, name: string, pages: number): Observable<any> {
    return this.http.patch(API_URL + 'update/' + id,{
      name,
      pages
    }, httpOptions);
  }

  delete(id : number): Observable<any> {
    return this.http.delete(API_URL + 'delete/' + id, { responseType : 'json' });
  }

}
