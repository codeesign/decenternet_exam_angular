import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { BookService } from '../../_services/book.service';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {


  form: any = {
    name: null,
    pages: null,
  };

  bookId : any;

  isSuccessful = false;
  isFailed = false;
  errorMessage = '';

  constructor(private bookService : BookService, private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit(): void {

    combineLatest(this._route.params, this._route.queryParams).subscribe(
      ([params,queryParams]) => {
        
        console.log(params, queryParams);

        if(params.id){
          this.bookId = params.id;
          this.getBook();
        }
       
       
    });
  }

  getBook () {
    this.bookService.get(this.bookId).subscribe((response : any) => {
        this.form = {
          name: response.name,
          pages: response.pages,
        };
    });
  }


  onSubmit(): void {
    const { name, pages } = this.form;

    if(this.bookId){
      this.bookService.update(this.bookId, name, pages).subscribe(
        (data : any) => {
          this.isSuccessful = true;
          this.isFailed = false;
        },
        (err : any) => {
          this.errorMessage = err.error.message;
          this.isFailed = true;
        }
      );
    }else{
      this.bookService.create(name, pages).subscribe(
        data => {
          console.log(data);
          this.isSuccessful = true;
          this.isFailed = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isFailed = true;
        }
      );
    }

    
  }
    


}
