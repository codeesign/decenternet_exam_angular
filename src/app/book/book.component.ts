import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { BookService } from '../_services/book.service';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  page: number = 1;
  limit: number = 5;

  bookLists = [];

  totalRecords: number = 0;

  paginator : any[] = [];

  constructor(
    private bookService : BookService, 
    private _router: Router,
    private _route: ActivatedRoute) 
    {

    }

  ngOnInit(): void {

    combineLatest(this._route.params, this._route.queryParams).subscribe(
      ([params,queryParams]) => {

        this.page = queryParams.page ? parseInt(queryParams.page) : this.page;
        this.limit = queryParams.limit ? parseInt(queryParams.limit) : this.limit;
       
        this.getBooks();
    });
    
  }


  getBooks () {
    this.bookService.getAll({ page : this.page, limit : this.limit }).subscribe(response => {

        // console.log(response);
        let data = response;
        this.totalRecords = data.count;
        this.bookLists = data.rows

        this.updatePagination();
    });
  }

  updatePagination() {
    this.paginator = [];
    let partitions = Math.ceil(this.totalRecords / this.limit);

    for (let index = 0; index < partitions; index++) {
      this.paginator.push(index+1)
    }

    console.log(this.paginator);

  }

  delete(id: number){

    let isConfirmed = confirm('Are you sure you want to delete this book?');

    if(isConfirmed){
      this.bookService.delete(id).subscribe((response) => {
        alert(response.message);
        window.location.reload();
      });
    }
    
    
  }

  onPage(page : number): void {
  
    this.page = page;

    this._router.navigate([`/book`], {queryParams : {page : this.page, limit : this.limit}});
  }
  

}
